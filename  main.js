// $(document).ready(function() { 
//     $(window).scroll(function(){
//         if(this.scrollY > 20) {
//             $('.navbar').addClass("sticky");
//         } else {
//             $('.navbar').removeClass("sticky");
//         }
//     });

//     //toggle menu/navbar script
//     $('.menu-btn').click(function(){
//         $('.navbar .menu').toggleClass("active")
//         $('.menu-btn i').toggleClass("active")
//     });

//     // owl carousel script
//     $('.carousel').owlCarousel({ 
//         margin: 20,
//         loop: true,
//         autoplayTimeOut: 2000,
//         autoplayHoverPause: true, 
//         responsive: {
//             0:{
//                items: 1,
//                nav: false, 
//             },
//             600:{
//                 items: 2,
//                 nav: false, 
//             },
//             1000:{
//                 items: 3 ,
//                 nav: false, 
//             }
//         }
//     })
// });


let modeBtn = document.getElementById("dark-light");

modeBtn.addEventListener("click", function () {
  if (document.body.className != "dark") {
    this.firstElementChild.src = "img/light.svg";
  } else {
    this.firstElementChild.src = "img/dark.svg";
  }
  document.body.classList.toggle("dark");
});




'use strict'

const menuToggle = document.querySelector('.menu-toggle');
const bxMenu = document.querySelector('.bx-menu');
const bxX = document.querySelector('.bx-x');

const navBar = document.querySelector('.navbar');

// --- open menu ---

bxMenu.addEventListener('click', (e)=> {
    if(e.target.classList.contains('bx-menu')){
        navBar.classList.add('show-navbar');
        bxMenu.classList.add('hide-bx');
        bxX.classList.add('show-bx');
    }
})

// --- close menu ---

bxX.addEventListener('click', (e)=> {
    if(e.target.classList.contains('bx-x')){
        navBar.classList.remove('show-navbar');
        bxMenu.classList.remove('hide-bx');
        bxX.classList.remove('show-bx');
    }
})

window.addEventListener('scroll', function() {
    const header = document.querySelector('header');
    header.classList.toggle('sticky', window.scrollY > 50)
})







$(".owl-carousel-1").owlCarousel({
  loop: true,
  nav: true,
  dots: true,
  margin: 20,
  responsiveClass: true,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 2,
    },
    1000: {
      items: 3,
    },
  },
});
